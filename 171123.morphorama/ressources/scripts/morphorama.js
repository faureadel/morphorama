
function drawLine(points,id,size,zIndex){

	if(document.getElementById(id)){
		var line = document.getElementById(id);
		updateAttributes(line,{
			x1:points[0][0],
			y1:points[0][1],
			x2:points[1][0],
			y2:points[1][1],
			stroke:"orange",
			'stroke-width':size/4+'px',
			'stroke-linecap':'round',
		});
		updateCSS(line.parentNode,{
			zIndex:zIndex-1
		});
		
		updateCSS(line,{
			zIndex:zIndex-1
		});
	}else{
		var svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
		updateCSS(svg,{
			width:'100%',
			height:'100%',
		});
		var line = document.createElementNS('http://www.w3.org/2000/svg', 'line');
		line.id = id
		svg.appendChild(line);
		document.body.appendChild(svg);
	}

}

function w(){
	return window.innerWidth;
}
function h(){
	return window.innerHeight;
}
function cx(){
	return Math.floor(w()/2);
}
function cy(){
	return Math.floor(h()/2);
}

Keys = function(){
}

Keys.prototype.newKey = function(key){
	this[key]=true;
}

keys = new Keys();


function rotate2d(pos,rad){
	var x = pos[0];
	var y = pos[1];

	var s = Math.sin(rad);
	var c = Math.cos(rad);

	return [x*c - y*s,y*c + x*s]
}

Cam = function(options){
	define(this,options,{
		pos:[0,0,0],
		scale:[0,0,1],
		rot:[0,0]
	});
}

Cam.prototype.events = function(event) {
	if(event.type=='mousemove'){
		var x = event.movementX;
		var y = event.movementY;
		x/=cx(); 
		y/=cy();
		this.rot[0]-=y*this.scale[2]; this.rot[1]-=x*this.scale[2];
	}
};

Cam.prototype.update = function(s,key) {



	var x = s*Math.sin(this.rot[1])*this.scale[2];
	var y = s*Math.cos(this.rot[1])*this.scale[2];
	var xZ = s*Math.sin(this.rot[0])*this.scale[2];
	var yZ = s*Math.cos(this.rot[0])*this.scale[2];


//console.log(this.rot[0],xZ);

if (key['a']){ this.pos[1]-=s*this.scale[2]};
if (key['e']){ this.pos[1]+=s*this.scale[2]};

if (key['z']){ this.pos[0]+=x; this.pos[2]+=y ;/* this.pos[1]+=xZ*/};
if (key['s']){ this.pos[0]-=x; this.pos[2]-=y ; /* this.pos[1]-=xZ*/};

if (key['q']){ this.pos[0]-=y; this.pos[2]+=x ; /*this.pos[1]-=yZ*/};
if (key['d']){ this.pos[0]+=y; this.pos[2]-=x ; /*this.pos[1]+=yZ*/};

if (key['+']){ this.scale[2]/=1+s/3;};
if (key['-']){ this.scale[2]*=1+s/3};

if (key['ArrowUp']){ this.scale[1]+=s*50*this.scale[2];};
if (key['ArrowDown']){ this.scale[1]-=s*50*this.scale[2];};

if (key['ArrowLeft']){ this.scale[0]+=s*50*this.scale[2];};
if (key['ArrowRight']){ this.scale[0]-=s*50*this.scale[2];};

if (key['Backspace']){ 
	if(this.scale[2]>1){
		this.scale[2]/=1+s/3;
	}
	if(this.scale[2]<1){
		this.scale[2]*=1+s/3;
	}
	this.scale[0]-=this.scale[0]*0.1;
	this.scale[1]-=this.scale[1]*0.1
};

}

Cam.prototype.spatialize = function(node){

	var x=node.coord[0], y=node.coord[1], z=node.coord[2];

	x-=this.pos[0];
	y-=this.pos[1];
	z-=this.pos[2];

	hRot = rotate2d([x,z],this.rot[1])
	vRot = rotate2d([y,hRot[1]],this.rot[0])

	var f = cx()/vRot[1];
	x = hRot[0]*f;
	y = vRot[0]*f;

	var zIndex = -parseInt(vRot[1]*nodes.length);
	dist=Math.sqrt(Math.pow(this.pos[0]-node.coord[0],2)+Math.pow(this.pos[1]-node.coord[1],2)+Math.pow(this.pos[2]-node.coord[2],2));
	size=w()/100*(node.size/dist);
	x=this.scale[0]-(this.scale[0]-x+this.scale[0]*this.scale[2])/this.scale[2];
	y=this.scale[1]-(this.scale[1]-y+this.scale[1]*this.scale[2])/this.scale[2];

	size/=this.scale[2];

	node.display={x:cx()+x,y:cy()+y,z:vRot[1],dist:dist,size:size,zIndex:zIndex};

}

var cam = new Cam({pos:[0,0,-5]});

var nodes=[];

Node = function(options){
	define(this,options,{
		cam:cam,
		dom:document.createElement('div'),
		display:{x:0,y:0,z:0,dist:0,size:0,zIndex:0},
		id:nodes.length,
		coord:[0,0,0],
		color:'transparent',
		size:100,
		image:undefined,
		descr:'',
	});
	//console.log(this.coord);
	nodes.push(this);
}

var viewMode=0;

Node.prototype.updateDom = function(){

	this.cam.spatialize(this);

	if(this.display.z>0.1){

		if(viewMode==0){

			updateCSS(document.body,{
				backgroundColor:'black'
			});
			updateCSS(this.dom,{
				marginLeft:this.display.x-this.display.size/2+'px',
				marginTop:this.display.y-this.display.size/2+'px',
				width:this.display.size+'px',
				height:this.display.size+'px',
				fontSize:this.display.size/2+'px',
				zIndex:this.display.zIndex,
				'background-image':'url('+this.image+')',
				backgroundColor:'transparent',
				'border-radius': '0'
			});
		}
		if(viewMode==1){

			updateCSS(document.body,{
				backgroundColor:'white'
			});

			updateCSS(this.dom,{
				marginLeft:this.display.x-this.display.size/4+'px',
				marginTop:this.display.y-this.display.size/4+'px',
				width:this.display.size/2+'px',
				height:this.display.size/2+'px',
				fontSize:this.display.size+'px',
				'background-image':'none',
				backgroundColor:this.color,
				zIndex:this.display.zIndex,
				'border-radius': '50%'
			});

		}
	}else{
		updateCSS(this.dom,{
			'background-image':'none',
			backgroundColor:'transparent',
		});
	}

}


console.log(nodes);
var reach=[false,false,false,false,false,false,false,false];
var reachPoints=[[0,0,-5,0,0,0,0,0]];
var reachIndex=0;

window.repeat = setInterval(function(){
	
	for (var i in cam.pos){
		if(reach[i]){
			if(cam.pos[i]!=reachPoints[reachIndex][i]){

				cam.pos[i]+=(reachPoints[reachIndex][i]-cam.pos[i])/25;

				if(reachPoints[reachIndex][i]-cam.pos[i]>0){
					if(reachPoints[reachIndex][i]-cam.pos[i]<0.01){

						reach[i]=false;
					}
				}else{
					if(reachPoints[reachIndex][i]-cam.pos[i]>-0.01){
						reach[i]=false;
					}
				}
			}
		}
	}

	for (var i in cam.scale){
		var index=parseInt(i)+5;

		if(reach[index]){
			if(cam.scale[i]!=reachPoints[reachIndex][index]){

				cam.scale[i]+=(reachPoints[reachIndex][index]-cam.scale[i])/25;

				if(reachPoints[reachIndex][index]-cam.scale[i]>0){
					if(reachPoints[reachIndex][index]-cam.scale[i]<0.01){

						reach[index]=false;
					}
				}else{
					if(reachPoints[reachIndex][index]-cam.scale[i]>-0.01){
						reach[index]=false;
					}
				}

			}
			
		}
	}

	for (var i in cam.rot){
		var index=parseInt(i)+3;

		if(reach[index]){
			if(cam.rot[i]!=reachPoints[reachIndex][index]){

				cam.rot[i]+=(reachPoints[reachIndex][index]-cam.rot[i])/25;

				if(reachPoints[reachIndex][index]-cam.rot[i]>0){
					if(reachPoints[reachIndex][index]-cam.rot[i]<0.01){

						reach[index]=false;
					}
				}else{
					if(reachPoints[reachIndex][index]-cam.rot[i]>-0.01){
						reach[index]=false;
					}
				}

			}
			
		}
	}
	

	cam.update(0.05,keys);

	if(viewMode==0){
		for(var i in nodes){
			nodes[i].updateDom();
		}
	}
	if(viewMode==1){
		for(var i in nodes){
			nodes[i].updateDom();
		}
	}
	

	//scroll
/*
	if(scroll>0){
console.log(scroll);
		if(wheelDelta>0){
			cam.update(0.05,{s:true,z:false});
			//cam.pos[2]-=scroll/100;
		}
		if(wheelDelta<0){
			cam.update(0.05,{z:true,s:false});
			//cam.pos[2]+=scroll/50;
		}
		scroll=0;
	}
	*/
	//console.log(cam.pos);

},17)

document.onkeydown = function(e){
	if(keys[e.key]==undefined){
		keys.newKey(e.key);
	}else{
		keys[e.key]=true;
	}

	console.log(e.keyCode);


}

document.onkeyup = function(e){
	console.log(e.key);
	if(keys[e.key]==undefined){
		keys.newKey(e.key);
	}else{
		keys[e.key]=false;
	}
	if(e.keyCode==32){
		reachPoints.push([cam.pos[0],cam.pos[1],cam.pos[2],cam.rot[0],cam.rot[1]]);
		reachIndex++;
		//console.log(cam.pos,cam.rot);
	}

}

var mousedown = false;

var scroll=0;

var wheelDelta=0;

document.onmousewheel =function(e){
	console.log(e);
	cam.events(e);
	scroll=scroll+2;
	if(e.wheelDelta>0){
		wheelDelta=1;
	}
	if(e.wheelDelta<0){
		wheelDelta=-1;
	}
	if(wheelDelta>0){
		cam.update(e.wheelDeltaY/1000,{s:true,z:false});
	}
	if(wheelDelta<0){
		cam.update(-e.wheelDeltaY/1000,{z:true,s:false});		
	}

}

document.onmousedown = function(e){
	if(mousedown){

	}else{
		mousedown=true
	}
}

document.onmouseup = function(e){
	if(mousedown){
		mousedown=false
	}
}



window.onmousemove = function(e){	
	if(mousedown){		
		cam.events(e);
	}
}

