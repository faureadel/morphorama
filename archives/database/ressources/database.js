var ensembles=[];
var elements=[];
var relations=[];
var metas={
	all:[],

};

Ensemble = function(options){
	this.metas=check('metas',options,[]);
	this.elements=[];
	ensembles.push(this);
}

Element = function(options){
	this.metas=check('metas',options,[]);
	this.ensemble=check('ensemble',options,null);
	this.relations=[];
	this.ensemble.elements.push(this);
}

Meta = function(options){
	this.type=check('type',options,null);
	this.content=check('content',options,null);
	metas.all.push(this);
	this.dom=document.createElement('div');
}

Meta.prototype.declare = function(){
	if(this.type!=null){declare(this,metas,this.type)};
	if(this.content!=null){declare(this,metas,this.content)};
}

Relation = function(options){
	this.elements=check('elements',options,null); 
	this.commentaires=check('metas',options,null);
}

var database =[
,{
	type:'date',
	contents:['Deuxième moitie du 20e siècle','1967','1977','1954','1974','1989','1998','19e' ,'1910','1999','1991','1986','1992','1945','1926','1929','1939','1991','1992','1948','1936']
},{
	type:'auteur',
	contents:['J. Bertin','Brad A. Myers','C. Beebe','E. Jacob','Christian von Ehrenfels','V. Rosenthal','Y.-M. Visetti','M. Wertheimer','W. Köhler','K. Koffka','Stephen E. Palmer','Shi-Kuo Chang','Edmond Husserl','Merleau-Ponty','Roman Jakobson','Louis Hjelmslev','Claude Lévi-Strauss','François Dosse','Claude Shannon','D. Rosenberg','A. Grafton','P. Dubourg Glatigny','H. Vérin','William Playfair','Otto Neurath','R. Kinross','Gerd Arntz','Marie Neurath']
},{
	type:'ouvrage',
	contents:['Sémiologie graphique','La Graphique','Taxinomies of Visual Programming and Program visualization','Graphic Language. Documents. Structures and Functions','Sens et temps de la Gestalt','Les théories contemporaines de la perception de la Gestalt','Introduction : Visual Languages and Iconic Language','L\'Histoire du structuralisme','A Mathematical Theory of Communication','Cartographie du temps. Des frises chronologiques aux nouvelles timelines','Réduire en art. La technologie de la Renaissance aux Lumieres','Empirism and Sociology','Le Transformateur. Principe de création des diagrammes Isotype']
},{
	type:'evenement',
	contents:['J. Bertin crée le Laboratoire de Cartographie ','rebaptisé en 1974 Laboratoire de Graphique, à l\'École des Hautes Études en Sciences Sociales, Paris.','Fondation du Cercle linguistique de Prague','Manifeste du Cercle de Prague','Fondation du Cercle de Copenhague','Création de la revue Acta Linguistica','le schéma de Roman Jakobson intègre la notion de communication, d\'émetteur et de récepteur du langage, et donc les idées d\'environnement et de but.','Claude Lévi-Strauss ouvre la voie au structuralisme dans sa discipline']
},{
	type:'discipline',
	contents:['Gestaltpsychologie','Gestalt Theorie','Phénoménologie','Structuralisme','Ethnologie']
},{
	type:'hyperlien',
	contents:['http://www.cs.cmu.edu/~bam/papers/vltax2.pdf','http://journals.lib.washington.edu/index.php/acro/article/view/12743','http://formes-symboliques.org/IMG/pdf/doc-79.pdf','http://intellectica.org/SiteArchives/archives/n28/28_03_Palmer.pdf','https://link.springer.com/chapter/10.1007%2F978-1-4613-1805-7_1','http://centenaire-shannon.cnrs.fr/chapter/la-theorie-de-information','https://link.springer.com/book/10.1007%2F978-94-010-2525-6','http://imaginarymuseum.org/MHV/PZImhv/NeurathPictureLanguage.html','http://www.hypergeo.eu/spip.php?article630','http://www.davidrumsey.com/luna/servlet/view/search/who/Dangeau,%20abbe%20de,%201643-1723/where/France/',]
},{
	type:'remarque',
	contents:[
	'"Le tout est plus que la somme de ses parties". Ont énoncé un certain nombre de principes (aussi appelés lois de la gestalt) dont : - La loi de proximité - La loi de fermeture - La loi de similarité - La loi de symétrie',
	'Précurseur : la Gestalt Psychologie',
	'ingénieur et économiste écossais'
	]
}
]



for (var i in database){
	//console.log(database[i]);
	for (var j in database[i].contents){
		var meta = new Meta({
			type:database[i].type,
		});
		meta.content=database[i].contents[j];
	}
}


for(var i in metas.all){
	metas.all[i].declare();
}
console.log(metas);
/*

var meta = new Meta({
	type:'auteur',
	content:'J.Bertin'
});

for ( var i = 0 ; i < 2 ; i++){
	var meta = new Meta({
		type:'ouvrage',
	});
	if(i==0){meta.content='Sémiologie graphique'};
	if(i==1){meta.content='La Graphique'};
}





*/

