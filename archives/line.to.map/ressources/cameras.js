var cameras = [];
var em=1;


var Camera = function(options){
	this.position = new Point({type:'camera',x:check('x',options,0),y:check('y',options,0),z:check('z',options,1)});
	this.target = new Point({type:'camera',x:check('targetX',options,window.innerWidth/4),y:check('targetY',options,window.innerHeight/2)});
console.log(this.target);
	this.speed = check('speed',options,4);
	this.traveling={
		left : false,
		up : false,
		right : false,
		down : false,
		front : false,
		back : false
	};
	cameras.push(this);
}

Camera.prototype.interface = function(e,bool){
	var keys=[37,38,39,40,107,109];
	var traveling=['left','up','right','down','front','back'];
	for(var i in keys){
		if(e.keyCode==keys[i]){                                         
			this.traveling[traveling[i]]=bool;

		}
	}

}

Camera.prototype.move = function(what){
	//console.log(this.target);
	//console.log('moving');
	var cam = this;
	var conduct = {
		left:function(){cam.position.x=cam.position.x-(1*cam.speed)/cam.position.z},
		up:function(){cam.position.y=cam.position.y-(1*cam.speed)/cam.position.z},
		right:function(){cam.position.x=cam.position.x+(1*cam.speed)/cam.position.z},
		down:function(){cam.position.y=cam.position.y+(1*cam.speed)/cam.position.z},
		front:function(){cam.position.z=cam.position.z*(1+cam.speed/350)},
		back:function(){cam.position.z=cam.position.z/(1+cam.speed/350)},
	};
	for (var i in this.traveling){
		if(this.traveling[i])
		{
			conduct[i]();
		}
	}
	for(var i in points.normal){
		this.update(points.normal[i]);
	}
}

Camera.prototype.tune = function(axis,value){

	return parseInt(this.target[axis]-(this.target[axis]-value+this.position[axis]*this.position.z)/this.position.z);
}

Camera.prototype.update = function(point){
	//console.log(this.target);
	//console.log(point.x);
	point.transfer={
		x:this.target['x']-(this.target['x']-point.x-this.position.x)*this.position.z*point.z,
		y:this.target['y']-(this.target['y']-point.y-this.position.y)*this.position.z,
	}
	if(point.transfer.x>this.target['x']){
		point.transfer.x=point.transfer.x+(point.transfer.x-this.target['x']);
	}else{
		point.transfer.x=point.transfer.x-(point.transfer.x-this.target['x']);
	}
	//point.z=point.z*this.position.z*1.001;
}
/*
Camera.prototype.update = function(what){
	for(var i in what){
		what[i].transfer={
			view:40/what[i].z*(what[i].z-this.position.z)/this.position.z/what[i].z,
			x:this.target['x']-(this.target['x']-what[i].x-this.position.x)*this.position.z*what[i].z,
			y:this.target['y']-(this.target['y']-what[i].y-this.position.y)*this.position.z*what[i].z,
			w:what[i].w*this.position.z*what[i].z,
			h:what[i].h*this.position.z*what[i].z,
			opacity:(this.position.z*what[i].z)/100
		}
		//console.log(what[i]);
		if(what[i].transfer.x + what[i].transfer.w > what[i].transfer.view && what[i].transfer.y + what[i].transfer.h > what[i].transfer.view && what[i].transfer.x < window.innerWidth-what[i].transfer.view && what[i].transfer.y < window.innerHeight-what[i].transfer.view && what[i].transfer.opacity<1){
			
			if(document.getElementById(what[i].name)==null){
				what[i].parent.appendChild(what[i].dom);
			}
			updateAttributes(what[i].dom,{
				viewBox:'0 0 '+what[i].transfer.w/(this.position.z*what[i].z)+' '+what[i].transfer.h/(this.position.z*what[i].z),
			})

			updateCSS(what[i].dom,{
				visibility:'visible',
				marginLeft:what[i].transfer.x+'px',
				marginTop:what[i].transfer.y+'px',
				width:what[i].transfer.w+'px',
				height:what[i].transfer.h+'px',
				opacity:1-what[i].transfer.opacity,
				zIndex:parseInt(what[i].z*100),
			});
			//console.log(what[i].dom.style);
		}else{
			if(document.getElementById(what[i].name)!=null){
				what[i].parent.removeChild(what[i].dom);
			}
		}
	}
}*/
var camera = new Camera();