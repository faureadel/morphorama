
var datas={
	all:[],
};

Data = function(options){
	this.type=check('type',options,null);
	this.content=check('content',options,null);
	this.view=check('view',options,null);
	datas.all.push(this);
}

Data.prototype.declare = function(){
	if(this.type!=null){declare(this,datas,this.type)};
	if(this.content!=null){declare(this,datas,this.content)};
}

var elements={
	all:[],
};

Element = function(options){
	this.type=check('type',options,null);
	this.datas=check('datas',options,[]);
	//this.view=check('view',options,null);
	elements.all.push(this);
}

Element.prototype.declare = function(){
	this.content='';
	for(var i in this.datas){
		this.content = this.content+this.datas[i].content+' ';
		//console.log(this.datas[i].dom)
	}
	if(this.type!=null){declare(this,elements,this.type)};
	if(this.content!=''){declare(this,elements,this.content)};
}

var database =[
,{
	type:'date',
	contents:['Deuxième moitie du 20e siècle','1967','1977','1954','1974','1989','1998','19e' ,'années 1910','1999','1991','1986','1992','1945','1926','1929','1939','1991','1992','1948','1936']
},{
	type:'auteur',
	contents:['Jacques Bertin','Brad A. Myers','C. Beebe','E. Jacob','Christian von Ehrenfels','V. Rosenthal','Y.-M. Visetti','M. Wertheimer','W. Köhler','K. Koffka','Stephen E. Palmer','Shi-Kuo Chang','Edmond Husserl','Merleau-Ponty','Roman Jakobson','Louis Hjelmslev','Claude Lévi-Strauss','François Dosse','Claude Shannon','D. Rosenberg','A. Grafton','P. Dubourg Glatigny','H. Vérin','William Playfair','Otto Neurath','R. Kinross','Gerd Arntz','Marie Neurath']
},{
	type:'ouvrage',
	contents:['Acta Linguistica','Phénoménologie de la perception','L\'idée de la phénoménologie','Sémiologie graphique','La Graphique','Taxinomies of Visual Programming and Program visualization','Graphic Language. Documents. Structures and Functions','Sens et temps de la Gestalt','Les théories contemporaines de la perception de la Gestalt','Introduction : Visual Languages and Iconic Language','L\'Histoire du structuralisme, tome 1','L\'Histoire du structuralisme, tome 2','A Mathematical Theory of Communication','Cartographie du temps. Des frises chronologiques aux nouvelles timelines','Réduire en art. La technologie de la Renaissance aux Lumieres','Empirism and Sociology','Le Transformateur. Principe de création des diagrammes Isotype']
},{
	type:'théories',
	contents:['Gestaltpsychologie','Gestalt Theorie','Phénoménologie','Structuralisme','Ethnologie']
},{
	type:'hyperlien',
	contents:['http://www.cs.cmu.edu/~bam/papers/vltax2.pdf','http://journals.lib.washington.edu/index.php/acro/article/view/12743','http://formes-symboliques.org/IMG/pdf/doc-79.pdf','http://intellectica.org/SiteArchives/archives/n28/28_03_Palmer.pdf','https://link.springer.com/chapter/10.1007%2F978-1-4613-1805-7_1','http://centenaire-shannon.cnrs.fr/chapter/la-theorie-de-information','https://link.springer.com/book/10.1007%2F978-94-010-2525-6','http://imaginarymuseum.org/MHV/PZImhv/NeurathPictureLanguage.html','http://www.hypergeo.eu/spip.php?article630','http://www.davidrumsey.com/luna/servlet/view/search/who/Dangeau,%20abbe%20de,%201643-1723/where/France/',]
},{
	type:'remarque',
	contents:['Copenhague','Allemagne','Autriche','France','Prague','philosophe français','philosophe autrichien','psychologues allemands','Jacques Bertin crée le Laboratoire de Cartographie rebaptisé en 1974 Laboratoire de Graphique, à l\'École des Hautes Études en Sciences Sociales, Paris.','Fondation du Cercle linguistique de Prague','Manifeste du Cercle de Prague','Fondation du Cercle de Copenhague','Création de la revue Acta Linguistica','le schéma de Roman Jakobson intègre la notion de communication, d\'émetteur et de récepteur du langage, et donc les idées d\'environnement et de but.','Claude Lévi-Strauss ouvre la voie au structuralisme dans sa discipline, l\'ethnologie','Le tout est plus que la somme de ses parties. Ont énoncé un certain nombre de principes (aussi appelés lois de la gestalt) dont : - La loi de proximité - La loi de fermeture - La loi de similarité - La loi de symétrie','Précurseur : la Gestalt Psychologie','ingénieur et économiste écossais']
}
]

for (var i in database){
	//console.log(database[i]);
	for (var j in database[i].contents){
		var data = new Data({
			type:database[i].type,
		});
		data.content=database[i].contents[j];
	}
}

for(var i in datas.all){
	datas.all[i].declare();
}



var ressources =[
{
	type:'étude sur :',
	datas:[
	[getDataByContent('Jacques Bertin'),getDataByContent('Sémiologie graphique'),getDataByContent('1967')],
	[getDataByContent('Jacques Bertin'),getDataByContent('La Graphique'),getDataByContent('1977')],
	[getDataByContent('Brad A. Myers'),getDataByContent('Taxinomies of Visual Programming and Program visualization'),getDataByContent('1989'),getDataByContent('http://www.cs.cmu.edu/~bam/papers/vltax2.pdf')],
	[getDataByContent('C. Beebe'),getDataByContent('E. Jacob'),getDataByContent('Graphic Language. Documents. Structures and Functions'),getDataByContent('1998'),getDataByContent('http://journals.lib.washington.edu/index.php/acro/article/view/12743')],
	[getDataByContent('V. Rosenthal'),getDataByContent('Y.-M. Visetti'),getDataByContent('Sens et temps de la Gestalt'),getDataByContent('1999'),getDataByContent('http://formes-symboliques.org/IMG/pdf/doc-79.pdf')],
	[getDataByContent('Stephen E. Palmer'),getDataByContent('Les théories contemporaines de la perception de la Gestalt'),getDataByContent('1991')],
	[getDataByContent('Shi-Kuo Chang'),getDataByContent('Introduction : Visual Languages and Iconic Language'),getDataByContent('1986'),getDataByContent('https://link.springer.com/chapter/10.1007%2F978-1-4613-1805-7_1')],
	[getDataByContent('François Dosse'),getDataByContent('L\'Histoire du structuralisme tome 1'),getDataByContent('L\'Histoire du structuralisme tome 2'),getDataByContent('1991'),getDataByContent('1992')],
	[getDataByContent('Claude Shannon'),getDataByContent('A Mathematical Theory of Communication'),getDataByContent('1948'),getDataByContent('http://centenaire-shannon.cnrs.fr/chapter/la-theorie-de-information')],
	[getDataByContent('D. Rosenberg'),getDataByContent('A. Grafton'),getDataByContent('Cartographie du temps. Des frises chronologiques aux nouvelles timelines'),getDataByContent('2013')],
	[getDataByContent('P. Dubourg Glatigny'),getDataByContent('H. Vérin'),getDataByContent('Réduire en art. La technologie de la Renaissance aux Lumieres'),getDataByContent('2008')],
	[getDataByContent('M. Neurath'),getDataByContent('R. Kinross'),getDataByContent('Le Transformateur. Principe de création des diagrammes Isotype'),getDataByContent('2013')],

	//[getDataByContent('C. Beebe'),getDataByContent('Taxinomies of Visual Programming and Program visualization'),getDataByContent('1989')],
	]
}
,{
	type:'discipline',
	datas:[
	[getDataByContent('Gestaltpsychologie'),getDataByContent('Christian von Ehrenfels'),getDataByContent('19e')],
	[getDataByContent('Gestalt Theorie'),getDataByContent('années 1910'),getDataByContent('Allemagne'),getDataByContent('M. Wertheimer'),getDataByContent('K. Koffka'),getDataByContent('M. Wertheimer'),getDataByContent('psychologues allemands '),getDataByContent('Le tout est plus que la somme de ses parties. Ont énoncé un certain nombre de principes (aussi appelés lois de la gestalt) dont : - La loi de proximité - La loi de fermeture - La loi de similarité - La loi de symétrie'),],
	[getDataByContent('1945'),getDataByContent('Phénoménologie de la perception'),getDataByContent('philosophe français'),getDataByContent('Merleau-Ponty'),getDataByContent('1992'),getDataByContent('L\'Idée de la phénoménologie'),getDataByContent('philosophe autrichien'),getDataByContent('Phénoménologie'),getDataByContent('Edmond Husserl')],
	]
}
,{
	type:'évenement',
	datas:[
	[getDataByContent('Prague'),getDataByContent('1926'),getDataByContent('Fondation du Cercle linguistique de Prague')],
	[getDataByContent('Prague'),getDataByContent('1929'),getDataByContent('Manifeste du Cercle de Prague')],
	[getDataByContent('Roman Jakobson'),getDataByContent('le schéma de Roman Jakobson intègre la notion de communication, d\'émetteur et de récepteur du langage, et donc les idées d\'environnement et de but.')],
	[getDataByContent('1939'),getDataByContent('Copenhague'),getDataByContent('fondation du Cercle de Copenhague')],
	[getDataByContent('Acta Linguistica'),getDataByContent('Louis Hjelmslev'),getDataByContent('Création de la revue Acta Linguistica')],
	[getDataByContent('Ethnologie'),getDataByContent('Deuxième moitie du 20e siècle'),getDataByContent('Claude Lévi-Strauss'),getDataByContent('Claude Lévi-Strauss ouvre la voie au structuralisme dans sa discipline, l\'ethnologie')],
	]
},{
	type:'personnalité',
	datas:[
	[getDataByContent('William Playfair'),getDataByContent('1926'),getDataByContent('Fondation du Cercle linguistique de Prague'),getDataByContent('ingénieur et économiste écossais')],
	]
},{
	type:'publication',
	datas:[
	[getDataByContent('Otto Neurath'),getDataByContent('Empirism and Sociology'),getDataByContent('https://link.springer.com/book/10.1007%2F978-94-010-2525-6')],
	[getDataByContent('Gerd Arntz'),getDataByContent('Marie Neurath'),getDataByContent('From Hieroglyphics to IsoType'),getDataByContent('2010')],
	]
}
]

for (var i in ressources){
	//console.log(ressources[i]);
	for (var j in ressources[i].datas){
		var element = new Element({
			type:ressources[i].type,
		});
		element.datas=ressources[i].datas[j];
	}
}

for(var i in elements.all){
	elements.all[i].declare();
}



/*

var data = new data({
	type:'auteur',
	content:'J.Bertin'
});

for ( var i = 0 ; i < 2 ; i++){
	var data = new data({
		type:'ouvrage',
	});
	if(i==0){data.content='Sémiologie graphique'};
	if(i==1){data.content='La Graphique'};
}





*/

