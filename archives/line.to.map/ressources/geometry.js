var points = {all:[]};

Point = function(options){
	this.name = check('name',options,points.all.length);
	this.type = check('type',options,'normal');
	this.x = check('x',options,0);
	this.y = check('y',options,0);
	this.z = check('z',options,1);
	points.all.push(this);
	points[this.name]=this;
	if(this.type!=null){declare(this,points,this.type)};
	return this;
}

var elements = {all:[]};

Element = function(options){
	this.name = check('name',options,elements.all.length);
	this.type = check('type',options,null);
	this.position = check('position',options,null);
	this.parent = check('parent',options,document.body.getElementsByTagName('main')[0]);
	this.dom=document.createElement('div');
	this.dom.style.zIndex=elements.all.length;
	this.dom.style.position='absolute';
	this.dom.id=this.name;
	this.dom.className='element';
	elements.all.push(this);
	elements[this.name]=this;
	if(this.type!=null){declare(this,elements,this.type)};
	this.parent.appendChild(this.dom);
}

var lines = {all:[]};

Line = function(options){
	this.name = check('name',options,lines.all.length);
	this.type = check('type',options,null);
	this.points = check('points',options,[]);
	this.parent = check('parent',options,document.body.getElementsByTagName('svg')[0]);
	this.dom = document.createElementNS('http://www.w3.org/2000/svg', 'path');
	this.dom.style.zIndex=lines.all.length;
	this.dom.style.position='absolute';
	this.dom.id=this.name;
	this.stroke='rgb('+random(0,255)+','+random(0,255)+','+random(0,255)+')';
	this.dom.className='line';
	lines.all.push(this);
	lines[this.name]=this;
	if(this.type!=null){declare(this,lines,this.type)};
	this.parent.appendChild(this.dom);
}

Line.prototype.update = function(options){
	var path='M';
	//console.log(this.points);
	for(var i = 0 ; i<this.points.length; i++){
		path=path+' '+this.points[i].transfer.x+' ';

		path=(i != this.points.length)?path+this.points[i].transfer.y+' L':path+this.points[i].transfer.y;

	}
if(path.charAt(path.length-1)=='L'){
		var expectedPath='';
		for(var i = 0 ; i < path.length-1 ; i++){
			expectedPath=expectedPath+path.charAt(i);
		}
	}
	path=expectedPath;
	//console.log(parseInt(1*bigger([this.points[0].z,this.points[this.points.length-1].z])*camera.position.z*100));
	updateCSS(this.dom,{
		strokeWidth:2*bigger([this.points[0].z,this.points[this.points.length-1].z])*camera.position.z+'px',
		zIndex:parseInt(2*bigger([this.points[0].z,this.points[this.points.length-1].z])*camera.position.z*1000),
	});
	updateAttributes(this.dom,{
		d:path,
		stroke:this.stroke,
		'stroke-width':10,
		
	})
}