/*
function drawLine(points,id,size,zIndex){

	if(document.getElementById(id)){
		var line = document.getElementById(id);
		updateAttributes(line,{
			x1:points[0][0],
			y1:points[0][1],
			x2:points[1][0],
			y2:points[1][1],
			stroke:"orange",
			'stroke-width':size/4+'px',
			'stroke-linecap':'round',
		});
		updateCSS(line.parentNode,{
			zIndex:zIndex-1
		});
		
		updateCSS(line,{
			zIndex:zIndex-1
		});
	}else{
		var svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
		updateCSS(svg,{
			width:'100%',
			height:'100%',
		});
		var line = document.createElementNS('http://www.w3.org/2000/svg', 'line');
		line.id = id
		svg.appendChild(line);
		document.body.appendChild(svg);
	}

}
*/
function w(){
	return window.innerWidth;
}
function h(){
	return window.innerHeight;
}
function cx(){
	return Math.floor(w()/2);
}
function cy(){
	return Math.floor(h()/2);
}

Keys = function(){
}

Keys.prototype.newKey = function(key){
	this[key]=true;
}

keys = new Keys();


function rotate2d(pos,rad){

	var x = pos[0];
	var y = pos[1];

	var s = Math.sin(rad);
	var c = Math.cos(rad);

	return [x*c - y*s,y*c + x*s]
}

Cam = function(options){
	define(this,options,{
		pos:[0,0,0],
		scale:[0,0,1],
		rot:[0,0]
	});
}

Cam.prototype.events = function(event) {
	if(event.type=='mousemove'){
		var x = event.movementX;
		var y = event.movementY;
		x/=cx(); 
		y/=cy();
		this.rot[0]-=y*this.scale[2]; this.rot[1]-=x*this.scale[2];
	}
};

var reach=[false,false,false,false,false];
var reachPoints=[[0,0,-5,0,0]];
var reachIndex=0;

Cam.prototype.update = function(s,key) {

	//key control

	var x = s*Math.sin(this.rot[1])*this.scale[2];
	var y = s*Math.cos(this.rot[1])*this.scale[2];
	var xZ = s*Math.sin(this.rot[0])*this.scale[2];
	var yZ = s*Math.cos(this.rot[0])*this.scale[2];

	if (key['a']){ this.pos[1]-=s*this.scale[2]};
	if (key['e']){ this.pos[1]+=s*this.scale[2]};

	if (key['z']){ this.pos[0]+=x; this.pos[2]+=y ; this.pos[1]+=xZ};
	if (key['s']){ this.pos[0]-=x; this.pos[2]-=y ; this.pos[1]-=xZ};

	if (key['q']){ this.pos[0]-=y; this.pos[2]+=x ;};
	if (key['d']){ this.pos[0]+=y; this.pos[2]-=x ;};

	if (key['+']){ this.scale[2]/=1+s/3;};
	if (key['-']){ this.scale[2]*=1+s/3};

	if (key['ArrowUp']){ this.scale[1]+=s*50*this.scale[2];};
	if (key['ArrowDown']){ this.scale[1]-=s*50*this.scale[2];};

	if (key['ArrowLeft']){ this.scale[0]+=s*50*this.scale[2];};
	if (key['ArrowRight']){ this.scale[0]-=s*50*this.scale[2];};

	if (key['Backspace']){ 
		if(this.scale[2]>1){
			this.scale[2]/=1+s/3;
		}
		if(this.scale[2]<1){
			this.scale[2]*=1+s/3;
		}
		this.scale[0]-=this.scale[0]*0.1;
		this.scale[1]-=this.scale[1]*0.1
	};

		// reach point
		for (var i in this.pos){
			if(reach[i]){
				if(this.pos[i]!=reachPoints[reachIndex][i]){

					this.pos[i]+=(reachPoints[reachIndex][i]-this.pos[i])/25;

					if(reachPoints[reachIndex][i]-this.pos[i]>0){
						if(reachPoints[reachIndex][i]-this.pos[i]<0.01){

							reach[i]=false;
						}
					}else{
						if(reachPoints[reachIndex][i]-this.pos[i]>-0.01){
							reach[i]=false;
						}
					}
				}
			}
		}

		for (var i in this.rot){
			var index=parseInt(i)+3;

			if(reach[index]){
				if(this.rot[i]!=reachPoints[reachIndex][index]){

					this.rot[i]+=(reachPoints[reachIndex][index]-this.rot[i])/25;

					if(reachPoints[reachIndex][index]-this.rot[i]>0){
						if(reachPoints[reachIndex][index]-this.rot[i]<0.01){

							reach[index]=false;
						}
					}else{
						if(reachPoints[reachIndex][index]-this.rot[i]>-0.01){
							reach[index]=false;
						}
					}

				}

			}
		}
	}

	Cam.prototype.spatialize = function(node){

		var x=node.coord[0], y=node.coord[1], z=node.coord[2];

		x-=this.pos[0];
		y-=this.pos[1];
		z-=this.pos[2];

		hRot = rotate2d([x,z],this.rot[1])
		vRot = rotate2d([y,hRot[1]],this.rot[0])

		var f = cx()/vRot[1];
		x = hRot[0]*f;
		y = vRot[0]*f;

		var zIndex = -parseInt(vRot[1]*nodes.length);
		dist=Math.sqrt(Math.pow(this.pos[0]-node.coord[0],2)+Math.pow(this.pos[1]-node.coord[1],2)+Math.pow(this.pos[2]-node.coord[2],2));
		size=w()/100*(node.size/dist);
		x=this.scale[0]-(this.scale[0]-x+this.scale[0]*this.scale[2])/this.scale[2];
		y=this.scale[1]-(this.scale[1]-y+this.scale[1]*this.scale[2])/this.scale[2];

		size/=this.scale[2];

		node.display={x:cx()+x,y:cy()+y,z:vRot[1],dist:dist,size:size,zIndex:zIndex};

	}

	var cam = new Cam({pos:[0,0,0]});

	var nodes=[];

	Node = function(options){
		define(this,options,{
			cam:cam,
			dom:document.createElementNS('http://www.w3.org/2000/svg', 'circle'),
			display:{x:0,y:0,z:0,dist:0,size:0,zIndex:0},
			id:nodes.length,
			coord:[0,0,0],
			color:'transparent',
			size:0,
			image:undefined,
			descr:'',
		});
		this.dom.className='node';
		this.dom.textContent=this.descr;
		console.log(this.coord);
		nodes.push(this);
	}

	var viewMode=1;

	Node.prototype.updateDom = function(){

		this.cam.spatialize(this);

		if(this.display.z>0.1){

			updateAttributes(this.dom,{
				fill:"red",
				cx:this.display.x,
				cy:this.display.y,
				r:5
			});
			
		}
	}

	console.log(nodes);

	var mode='camera'

	window.repeat = setInterval(function(){

	//nodes

		for(var i in nodes){
			nodes[i].updateDom();
		}


	//camera

	if(mode=='camera'){

		cam.update(0.05,keys);

		if(scroll>0){
			if(wheelDelta>0){
				cam.update(0.1,{s:true,z:false});
			}
			if(wheelDelta<0){
				cam.update(0.1,{z:true,s:false});
			}
			scroll=0;
		}
	}

},17)

	document.onkeydown = function(e){

		if(keys[e.key]==undefined){
			keys.newKey(e.key);
		}else{
			keys[e.key]=true;
		}

		console.log(e.keyCode);

	}

	document.onkeyup = function(e){

		if(keys[e.key]==undefined){
			keys.newKey(e.key);
		}else{
			keys[e.key]=false;
		}


	}

	var mousedown = false;

	var scroll=0;

	var wheelDelta=0;

	document.onmousewheel =function(e){
		cam.events(e);
		scroll=scroll+2;
		if(e.wheelDelta>0){
			wheelDelta=1;
		}
		if(e.wheelDelta<0){
			wheelDelta=-1;
		}

	}

	document.onmousedown = function(e){
		if(mousedown){

		}else{
			mousedown=true
		}
	}

	document.onmouseup = function(e){
		if(mousedown){
			mousedown=false
		}
	}



	window.onmousemove = function(e){	
		if(mousedown){		
			cam.events(e);
		}
	}

