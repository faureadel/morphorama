<!DOCTYPE html>
<html>
<head>
	<title></title>
	<script src="ressources/scripts/jtax.js"></script>
	<script src="ressources/scripts/morphorama.save-load_views.js"></script>
	<style>
	html{
		overflow: hidden;
		font-size: 2.5vw;
		/*background-color: black;*/
	}
	*{
		position: absolute;
		margin: 0;
		padding: 0;
		background-repeat: no-repeat;
		background-position:center;
		background-size: 100%;
		

	}
	button{
		margin-top: 0.5rem;
		margin-left: 0.5rem;
		background-color: white;
		border:none;
		padding: 0.125rem;
		font-size: 0.5rem;
	}
	div{

		/*outline: solid;
		outline-width: 0.1px;*/
	}
</style>
</head>
<body>

	<?php
	$dir = 'ressources/images';
	$files = scandir($dir);
	echo "<div id='data' ";
	foreach ($files as $key => $value) {
		if($value == "." || $value == ".."){}else{
			echo $key."=\"".$dir."/".$value."\"";
		}
	}
	echo "></div>";
	?>
	<main>
	</main>
	<script>
		window.onload = function(){
			// data

			var data = document.getElementById('data');
			var imagesScr=[];

			for(var i in data.attributes ){
				if(data.attributes[i].value != undefined){
					if(data.attributes[i].value.includes('.png')){
						imagesScr.push(data.attributes[i].value);
					}
				}
			}

			console.log(imagesScr);

			//#morphorama

			//## camera interface
			function buttonsLength(){
				return document.getElementsByTagName('button').length;
			}
			
			var button=document.createElement('button');

			updateCSS(button,{
				'margin-top':0.5+buttonsLength()+'rem',
			})

			button.onclick=function(){
				if(viewMode<1){
					viewMode++;
				}else{
					viewMode=0;
				}
			}

			button.textContent='changeMode';

			document.body.appendChild(button);

			var button=document.createElement('button');

			updateCSS(button,{
				'margin-top':0.5+buttonsLength()+'rem',
			})

			var views = 1;

			button.onclick=function(){

				reachPoints.push([cam.pos[0],cam.pos[1],cam.pos[2],cam.rot[0],cam.rot[1],cam.scale[0],cam.scale[1],cam.scale[2]]);
				reachIndex=reachPoints.length-2;
				reachIndex++;

				var button=document.createElement('button');

				updateCSS(button,{
					'margin-top':0.5+buttonsLength()+'rem',
				})

				updateAttributes(button,{
					view:reachIndex,
				})

				button.onclick=function(){
					reachIndex=this.getAttribute('view');
					console.log();
					reach=[true,true,true,true,true,true,true,true];
				}

				button.textContent=views;
				document.body.appendChild(button);
				views++;

			}

			button.textContent='save_view';
			document.body.appendChild(button);

/*

		reachPoints.push([cam.pos[0],cam.pos[1],cam.pos[2],cam.rot[0],cam.rot[1]]);
		reachIndex++;
		*/

			//## creating nodes from data
			var x=0;
			var z=0;

			for(var i in imagesScr){

				if(i==5){
					x=5;
					z=0;
				}

				var node = new Node({color:'blue',coord:[x,random(-10,10).int,z*2],image:imagesScr[i]});
				z++;
			}

			for(var i = 0 ; i<nodes.length ; i++){

				document.body.appendChild(nodes[i].dom);

			}

		}
	</script>
</body>
</html>